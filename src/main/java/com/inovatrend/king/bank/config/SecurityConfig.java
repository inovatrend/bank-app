package com.inovatrend.king.bank.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

import com.inovatrend.king.bank.domain.Role;
import com.inovatrend.king.bank.domain.User;
import com.inovatrend.king.bank.service.UserManagerImpl;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String bankClerkUsername = "banka";

    private static final String bankClerkPassword = "banka";

    private UserManagerImpl userManagerImpl;

    public SecurityConfig(UserManagerImpl userManagerImpl) {
        this.userManagerImpl = userManagerImpl;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/user/register").permitAll()
                .antMatchers("/user/**").hasRole("USER")
                .antMatchers("/bank/**").hasRole("BANK")
                .and()
                .formLogin().loginPage("/login").failureUrl("/login-error");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userManagerImpl);

        User bankUser = new User(bankClerkUsername, passwordEncoder().encode(bankClerkPassword));
        bankUser.setFirstName("Banka");
        bankUser.setLastName("d.o.o.");
        bankUser.setRole(Role.ROLE_BANK);

        auth.inMemoryAuthentication()
                .withUser(bankUser);
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new StandardPasswordEncoder("secret");
    }

}