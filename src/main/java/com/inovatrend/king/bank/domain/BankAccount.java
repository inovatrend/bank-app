package com.inovatrend.king.bank.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.inovatrend.king.bank.dto.BankAccountDTO;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "bank_account")
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_account_seq")
    @SequenceGenerator(name = "bank_account_seq", sequenceName = "bank_account_seq", allocationSize = 10)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "iban")
    private String iban;

    @Column(name = "balance")
    private BigDecimal balance;

    public BankAccount(User user, String iban, BigDecimal balance) {
        this.user = user;
        this.iban = iban;
        this.balance = balance;
    }

    public BankAccountDTO toDTO() {
        return new BankAccountDTO(id, user.toDTO(), iban, balance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankAccount)) return false;

        BankAccount account = (BankAccount) o;

        if (user != null ? !user.equals(account.user) : account.user != null) return false;
        return iban != null ? iban.equals(account.iban) : account.iban == null;
    }

    @Override
    public int hashCode() {
        int result = user != null ? user.hashCode() : 0;
        result = 31 * result + (iban != null ? iban.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BankAccount{");
        sb.append("id=").append(id);
        sb.append(", user=").append(user);
        sb.append(", iban='").append(iban).append('\'');
        sb.append(", balance=").append(balance);
        sb.append('}');
        return sb.toString();
    }


}
