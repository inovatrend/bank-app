package com.inovatrend.king.bank.domain;

import lombok.Getter;
import lombok.Setter;

import com.inovatrend.king.bank.dto.BankTransactionDTO;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "bank_transaction")
public class BankTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_transaction_seq")
    @SequenceGenerator(name = "bank_transaction_seq", sequenceName = "bank_transaction_seq", allocationSize = 100)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "debtor_account_id")
    private BankAccount debtorAccount;

    @ManyToOne
    @JoinColumn(name = "creditor_account_id")
    private BankAccount creditorAccount;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private TransactionStatus status;

    @Column(name = "created")
    private LocalDateTime created;

    public BankTransactionDTO toDTO() {
        return new BankTransactionDTO(id, debtorAccount.toDTO(), creditorAccount.toDTO(), amount, status, created);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankTransaction)) return false;

        BankTransaction that = (BankTransaction) o;

        if (debtorAccount != null ? !debtorAccount.equals(that.debtorAccount) : that.debtorAccount != null) return false;
        if (creditorAccount != null ? !creditorAccount.equals(that.creditorAccount) : that.creditorAccount != null) return false;
        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        return created != null ? created.equals(that.created) : that.created == null;
    }

    @Override
    public int hashCode() {
        int result = debtorAccount != null ? debtorAccount.hashCode() : 0;
        result = 31 * result + (creditorAccount != null ? creditorAccount.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BankTransaction{");
        sb.append("id=").append(id);
        sb.append(", debtorAccount=").append(debtorAccount);
        sb.append(", creditorAccount=").append(creditorAccount);
        sb.append(", amount=").append(amount);
        sb.append(", status=").append(status);
        sb.append(", created=").append(created);
        sb.append('}');
        return sb.toString();
    }

}
