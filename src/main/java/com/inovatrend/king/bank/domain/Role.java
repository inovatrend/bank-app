package com.inovatrend.king.bank.domain;

public enum Role {

    ROLE_USER,
    ROLE_BANK
}
