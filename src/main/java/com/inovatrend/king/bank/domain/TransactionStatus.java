package com.inovatrend.king.bank.domain;

public enum TransactionStatus {
    PENDING, EXECUTED, REJECTED
}
