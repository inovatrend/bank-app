package com.inovatrend.king.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BankAccountDTO {

    private Long id;

    private UserDTO user;

    private String iban;

    private BigDecimal balance;

}
