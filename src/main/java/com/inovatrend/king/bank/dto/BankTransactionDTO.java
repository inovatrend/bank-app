package com.inovatrend.king.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.inovatrend.king.bank.domain.TransactionStatus;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BankTransactionDTO {

    private Long id;

    @NotNull
    private BankAccountDTO debtorAccount;

    @NotNull
    private BankAccountDTO creditorAccount;

    @NotNull
    private BigDecimal amount;

    private TransactionStatus status;

    private LocalDateTime created;


}
