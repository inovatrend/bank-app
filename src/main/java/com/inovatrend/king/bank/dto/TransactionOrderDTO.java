package com.inovatrend.king.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionOrderDTO {

    @NotNull(message = "Iban has to be specified")
    @Size(min = 10, max = 10, message = "Iban must be exactly 10 characters long")
    private String creditorIban;

    @NotNull(message = "Amount has to be specified")
    @Digits(integer = 6, fraction = 2)
    @DecimalMin("0.00")
    private BigDecimal amount;

}
