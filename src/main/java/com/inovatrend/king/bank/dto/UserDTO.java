package com.inovatrend.king.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private Long id;

    @NotNull
    @Size(min = 4, max = 12, message = "Username must be between 4 and 12 characters long")
    private String username;

    @NotNull
    @Size(min = 6, max = 12, message = "Password must be between 6 and 12 characters long")
    private String password;

    @NotNull
    @Size(min = 1, max = 20, message = "First name must be between 1 and 20 characters long")
    private String firstName;

    @NotNull
    @Size(min = 1, max = 20, message = "Last name must be between 1 and 20 characters long")
    private String lastName;

}
