package com.inovatrend.king.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inovatrend.king.bank.domain.BankAccount;
import com.inovatrend.king.bank.domain.User;

import java.util.Optional;

public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    BankAccount findByUser(User user);

    Optional<BankAccount> findByIban(String iban);
}
