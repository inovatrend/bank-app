package com.inovatrend.king.bank.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.inovatrend.king.bank.domain.BankTransaction;
import com.inovatrend.king.bank.domain.TransactionStatus;
import com.inovatrend.king.bank.domain.User;

public interface BankTransactionRepository extends JpaRepository<BankTransaction, Long> {

    Page<BankTransaction> findByDebtorAccountUser(User user, Pageable page);

    Page<BankTransaction> findByDebtorAccountUserAndStatus(User user, TransactionStatus status, Pageable page);

    Page<BankTransaction> findByStatus(TransactionStatus status, Pageable page);

}
