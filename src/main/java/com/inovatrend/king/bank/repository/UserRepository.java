package com.inovatrend.king.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inovatrend.king.bank.domain.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

}
