package com.inovatrend.king.bank.service;

import com.inovatrend.king.bank.domain.BankAccount;
import com.inovatrend.king.bank.domain.User;

import java.math.BigDecimal;
import java.util.Optional;

public interface BankAccountManager {

    BigDecimal initialBalance = new BigDecimal(1000);

    BankAccount createAccount(User user);

    BankAccount getAccount(User user);

    Optional<BankAccount> getAccountByIban(String iban);


}