package com.inovatrend.king.bank.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import com.inovatrend.king.bank.domain.BankAccount;
import com.inovatrend.king.bank.domain.User;
import com.inovatrend.king.bank.repository.BankAccountRepository;

import java.util.Optional;

@Service
@Slf4j
public class BankAccountManagerImpl implements BankAccountManager {

    private final BankAccountRepository bankAccountRepository;

    public BankAccountManagerImpl(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    @Override
    public BankAccount createAccount(User user) {

        String iban = createIban();
        BankAccount account = new BankAccount(user, iban, initialBalance);
        account = bankAccountRepository.save(account);

        log.info("Bank account for user:{} successfully created.", user.getUsername());
        return account;

    }

    private String createIban() {
        String iban = RandomStringUtils.randomNumeric(10);
        Optional<BankAccount> savedAccount = bankAccountRepository.findByIban(iban);
        if (savedAccount.isPresent()) return createIban();
        return iban;
    }

    @Override
    public BankAccount getAccount(User user) {
        return bankAccountRepository.findByUser(user);
    }

    @Override
    public Optional<BankAccount> getAccountByIban(String iban) {
        return bankAccountRepository.findByIban(iban);
    }

}
