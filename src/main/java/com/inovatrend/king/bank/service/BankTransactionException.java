package com.inovatrend.king.bank.service;

public class BankTransactionException extends Exception {

    public BankTransactionException(String message) {
        super(message);
    }

}
