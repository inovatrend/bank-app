package com.inovatrend.king.bank.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.inovatrend.king.bank.domain.BankAccount;
import com.inovatrend.king.bank.domain.BankTransaction;
import com.inovatrend.king.bank.domain.TransactionStatus;
import com.inovatrend.king.bank.domain.User;

import java.math.BigDecimal;

public interface BankTransactionManager {

    Page<BankTransaction> getUserTransactions(User user, Pageable page);

    Page<BankTransaction> getUserTransactionsByStatus(User user, TransactionStatus status, Pageable page);

    Page<BankTransaction> getTransactionsByStatus(TransactionStatus status, Pageable page);

    Page<BankTransaction> getTransactions(Pageable page);

    BankTransaction transactionOrder(BankAccount debtorAccount, BankAccount creditorAccount, BigDecimal amount) throws BankTransactionException;

    BankTransaction commitTransaction(Long transactionId) throws BankTransactionException;

    BankTransaction rejectTransaction(Long transactionId) throws BankTransactionException;
}
