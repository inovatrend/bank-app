package com.inovatrend.king.bank.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.inovatrend.king.bank.domain.BankAccount;
import com.inovatrend.king.bank.domain.BankTransaction;
import com.inovatrend.king.bank.domain.TransactionStatus;
import com.inovatrend.king.bank.domain.User;
import com.inovatrend.king.bank.repository.BankAccountRepository;
import com.inovatrend.king.bank.repository.BankTransactionRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

@Service
@Slf4j
public class BankTransactionManagerImpl implements BankTransactionManager {

    private final BankTransactionRepository bankTransactionRepository;

    private final BankAccountRepository bankAccountRepository;

    private final TransactionHelper helper;

    private AtomicLong transactionCounter = new AtomicLong(0);

    private BigDecimal automaticCommitThreshold = new BigDecimal(100);

    private final ReentrantLock lock = new ReentrantLock();

    public BankTransactionManagerImpl(BankTransactionRepository bankTransactionRepository, BankAccountRepository bankAccountRepository, TransactionHelper helper) {
        this.bankTransactionRepository = bankTransactionRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.helper = helper;
    }

    @Override
    public Page<BankTransaction> getUserTransactions(User user, Pageable page) {
        return bankTransactionRepository.findByDebtorAccountUser(user, page);
    }

    @Override
    public Page<BankTransaction> getUserTransactionsByStatus(User user, TransactionStatus status, Pageable page) {
        return bankTransactionRepository.findByDebtorAccountUserAndStatus(user, status, page);
    }

    @Override
    public Page<BankTransaction> getTransactionsByStatus(TransactionStatus status, Pageable page) {
        return bankTransactionRepository.findByStatus(status, page);
    }

    @Override
    public Page<BankTransaction> getTransactions(Pageable page) {
        return bankTransactionRepository.findAll(page);
    }

    @Override
    public BankTransaction transactionOrder(BankAccount debtorAccount, BankAccount creditorAccount, BigDecimal amount) throws BankTransactionException {
        try {
            lock.lock();
            log.info("Processing transaction order: debtor:{}, creditor:{}, amount:{}", debtorAccount, creditorAccount, amount);

            BigDecimal balance = getBalanceFromDb(debtorAccount);
            if (balance.compareTo(amount) < 0) {
                log.error("Debtor balance: {} is less then transaction amount: {}, cannot process transaction", balance, amount);
                throw new BankTransactionException("Not enough money on account to process transaction");
            }

            BankTransaction bankTransaction = new BankTransaction();
            bankTransaction.setAmount(amount);
            bankTransaction.setCreated(LocalDateTime.now());
            bankTransaction.setCreditorAccount(creditorAccount);
            bankTransaction.setDebtorAccount(debtorAccount);
            bankTransaction.setStatus(TransactionStatus.PENDING);

            bankTransaction = bankTransactionRepository.save(bankTransaction);

            if (shouldCommitAutomatically(bankTransaction)) {
                commitTransaction(bankTransaction.getId());
            }

            return bankTransaction;
        } finally {
            lock.unlock();
        }
    }

    private BigDecimal getBalanceFromDb(BankAccount debtorAccount) throws BankTransactionException {
        Optional<BankAccount> account = bankAccountRepository.findById(debtorAccount.getId());
        BigDecimal balance;
        if (account.isPresent()) {
            balance = account.get().getBalance();
        } else {
            throw new BankTransactionException("Unknown account");
        }
        return balance;
    }

    private boolean shouldCommitAutomatically(BankTransaction bankTransaction) {
        long count = transactionCounter.incrementAndGet();

        if (count % 10 == 0) return false;

        return bankTransaction.getAmount().compareTo(automaticCommitThreshold) < 0;
    }

    @Override
    public BankTransaction commitTransaction(Long transactionId) throws BankTransactionException {
        BankTransaction transaction = bankTransactionRepository.getOne(transactionId);
        if (transaction != null) {
            if (transaction.getStatus() == TransactionStatus.PENDING) {
                try {
                    lock.lock();
                    BigDecimal amount = transaction.getAmount();
                    BankAccount debtorAccount = transaction.getDebtorAccount();
                    BankAccount creditorAccount = transaction.getCreditorAccount();

                    BigDecimal debtorAccountBalance = getBalanceFromDb(debtorAccount);
                    if (debtorAccountBalance.compareTo(amount) < 0) {
                        log.error("Debtor balance: {} is less then transaction amount: {}, rejecting transaction {}", debtorAccountBalance, amount, transaction);
                        rejectTransaction(transaction);
                        throw new BankTransactionException("Not enough money on account to process transaction");
                    }

                    BankTransaction finalBankTransaction = transaction;
                    transaction = helper.withTransaction(() -> transferMoney(finalBankTransaction, amount, debtorAccount, creditorAccount));
                    return transaction;
                } finally {
                    lock.unlock();
                }
            } else {
                throw new BankTransactionException("Only transactions that are in PENDING state can be commited!");
            }
        } else
            throw new BankTransactionException("Transaction with provided id is not existing");
    }

    private BankTransaction transferMoney(BankTransaction bankTransaction, BigDecimal amount, BankAccount debtorAccount, BankAccount creditorAccount) {
        debtorAccount.setBalance(debtorAccount.getBalance().subtract(amount));
        creditorAccount.setBalance(creditorAccount.getBalance().add(amount));
        bankTransaction.setStatus(TransactionStatus.EXECUTED);

        bankAccountRepository.save(debtorAccount);
        bankAccountRepository.save(creditorAccount);
        bankTransaction = bankTransactionRepository.save(bankTransaction);
        log.info("Successfully executed transaction:{}", bankTransaction);
        return bankTransaction;
    }

    @Override
    public BankTransaction rejectTransaction(Long transactionId) throws BankTransactionException {
        BankTransaction transaction = bankTransactionRepository.getOne(transactionId);
        if (transaction == null) {
            throw new BankTransactionException("Transaction with provided id is not existing");
        } else {
            return rejectTransaction(transaction);
        }
    }

    private BankTransaction rejectTransaction(BankTransaction bankTransaction) {
        try {
            lock.lock();
            bankTransaction.setStatus(TransactionStatus.REJECTED);
            return bankTransactionRepository.save(bankTransaction);
        } finally {
            lock.unlock();
        }
    }

}
