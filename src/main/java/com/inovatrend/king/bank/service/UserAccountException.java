package com.inovatrend.king.bank.service;

public class UserAccountException extends Exception {

    public UserAccountException(String message) {
        super(message);
    }

}
