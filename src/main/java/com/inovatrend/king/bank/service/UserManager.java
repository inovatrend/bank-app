package com.inovatrend.king.bank.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.inovatrend.king.bank.domain.User;

import java.util.Optional;

public interface UserManager extends UserDetailsService {

    Page<User> getUsers(Pageable pageable);

    User save(User user);

    Optional<User> getUserById(Long userId);

    Optional<User> getUserByUsername(String username);

    void deleteUser(Long userId);

}
