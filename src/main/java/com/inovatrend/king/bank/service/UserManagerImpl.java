package com.inovatrend.king.bank.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.inovatrend.king.bank.domain.User;
import com.inovatrend.king.bank.repository.UserRepository;

import java.util.Optional;


@Service
public class UserManagerImpl implements UserManager {

    private final UserRepository repository;

    public UserManagerImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public Page<User> getUsers(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public User save(User user) {
        return repository.save(user);
    }

    @Override
    public Optional<User> getUserById(Long userId) {
        return repository.findById(userId);
    }

    @Override
    public Optional<User> getUserByUsername(String username) {
        return repository.findByUsername(username);
    }

    @Override
    public void deleteUser(Long userId) {
        repository.deleteById(userId);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = repository.findByUsername(username);
        if (!user.isPresent())
            throw new UsernameNotFoundException("No user with username: " + username);

        return user.get();
    }
}
