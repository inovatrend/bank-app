package com.inovatrend.king.bank.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.inovatrend.king.bank.domain.BankTransaction;
import com.inovatrend.king.bank.domain.TransactionStatus;
import com.inovatrend.king.bank.dto.BankTransactionDTO;
import com.inovatrend.king.bank.service.BankTransactionManager;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/bank")
@Slf4j
public class BankClerkTransactionController {

    private final BankTransactionManager bankTransactionManager;

    public BankClerkTransactionController(BankTransactionManager bankTransactionManager) {
        this.bankTransactionManager = bankTransactionManager;
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    @GetMapping("/transaction/list")
    public String listTransactions(@RequestParam("transactionStatus") Optional<TransactionStatus> transactionStatus,
                                   @RequestParam("page") Optional<Integer> page,
                                   Model model) {

        int currentPage = page.orElse(1);
        PageRequest pageRequest = TransactionsPageUtils.createPageRequest(currentPage);

        Page<BankTransaction> transactions;
        if (transactionStatus.isPresent()) {
            transactions = bankTransactionManager.getTransactionsByStatus(transactionStatus.get(), pageRequest);
        } else {
            transactions = bankTransactionManager.getTransactions(pageRequest);
        }

        List<BankTransactionDTO> transactionDTOS = transactions.stream().map(BankTransaction::toDTO).collect(Collectors.toList());
        model.addAttribute("transactions", transactionDTOS);
        model.addAttribute("transactionStatus", transactionStatus.orElse(null));

        int totalPages = transactions.getTotalPages();
        TransactionsPageUtils.addPageInfo(model, transactions, totalPages);

        model.addAttribute("selectedNavItem", NavigationItem.LIST_TRANSACTIONS);

        return "list-transactions";
    }


    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    @GetMapping("/transaction/execute")
    public String executeTransaction(@RequestParam("transactionStatus") Optional<TransactionStatus> transactionStatus,
                                     @RequestParam("transactionId") Long transactionId,
                                     @RequestParam("page") Optional<Integer> page,
                                     Model model, RedirectAttributes redirectAttrs) {
        try {
            bankTransactionManager.commitTransaction(transactionId);
            redirectAttrs.addFlashAttribute("infoMessage", "Transaction successfully executed!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            redirectAttrs.addFlashAttribute("errorMessage", e.getMessage());
        }

        return "redirect:/bank/transaction/list";
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    @GetMapping("/transaction/reject")
    public String rejectTransaction(@RequestParam("transactionStatus") Optional<TransactionStatus> transactionStatus,
                                    @RequestParam("transactionId") Long transactionId,
                                    @RequestParam("page") Optional<Integer> page,
                                    Model model, RedirectAttributes redirectAttrs) {
        try {
            bankTransactionManager.rejectTransaction(transactionId);
            redirectAttrs.addFlashAttribute("infoMessage", "Transaction successfully rejected!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            redirectAttrs.addFlashAttribute("errorMessage", e.getMessage());
        }

        return "redirect:/bank/transaction/list";
    }

}
