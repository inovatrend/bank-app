package com.inovatrend.king.bank.web;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.inovatrend.king.bank.domain.BankAccount;
import com.inovatrend.king.bank.domain.Role;
import com.inovatrend.king.bank.domain.User;
import com.inovatrend.king.bank.service.BankAccountManager;

@Controller
public class HomeController {


    private final BankAccountManager bankAccountManager;

    public HomeController(BankAccountManager bankAccountManager) {
        this.bankAccountManager = bankAccountManager;
    }

    @RequestMapping("/")
    public String showHomePage(Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.isAuthenticated()){
            Object principal = auth.getPrincipal();
            if (principal instanceof User){
                User user = (User) principal;
                if (user.getRole() == Role.ROLE_USER) {
                    BankAccount account = bankAccountManager.getAccount(user);
                    if (account != null){
                        model.addAttribute("bankAccount", account);
                    }
                }
            }
        }
        model.addAttribute("selectedNavItem", NavigationItem.HOME);
        return "home";
    }

    // Login form
    @RequestMapping("/login")
    public String login() {
        return "login.html";
    }

    // Login form with error
    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login.html";
    }

}
