package com.inovatrend.king.bank.web;

public enum NavigationItem {

    HOME,
    LIST_TRANSACTIONS,
    MAKE_TRANSACTION,
    USER_REGISTRATION
}
