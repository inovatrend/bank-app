package com.inovatrend.king.bank.web;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;

import com.inovatrend.king.bank.domain.BankTransaction;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class TransactionsPageUtils {

    private static final int pageSize = 10;

    static PageRequest createPageRequest(int currentPage) {
        return PageRequest.of(currentPage - 1, pageSize, Sort.by(Sort.Direction.ASC, "created"));
    }

    static void addPageInfo(Model model, Page<BankTransaction> transactions, int totalPages) {
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
            model.addAttribute("currentPage", transactions.getNumber() + 1);
        }
    }

}