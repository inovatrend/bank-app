package com.inovatrend.king.bank.web;


import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import com.inovatrend.king.bank.config.SecurityConfig;
import com.inovatrend.king.bank.domain.Role;
import com.inovatrend.king.bank.domain.User;
import com.inovatrend.king.bank.dto.UserDTO;
import com.inovatrend.king.bank.service.BankAccountManager;
import com.inovatrend.king.bank.service.UserManager;

import javax.validation.Valid;


@Controller
@RequestMapping("/user")
@SessionAttributes("user")
@Slf4j
public class UserRegistrationController {

    private final UserManager userManager;

    private final BankAccountManager bankAccountManager;

    private final PasswordEncoder passwordEncoder;

    public UserRegistrationController(UserManager userManager, BankAccountManager bankAccountManager, PasswordEncoder passwordEncoder) {
        this.userManager = userManager;
        this.bankAccountManager = bankAccountManager;
        this.passwordEncoder = passwordEncoder;
    }


    @GetMapping("/register")
    public String showRegisterUserForm(Model model) {
        model.addAttribute("user", new UserDTO());
        model.addAttribute("selectedNavItem", NavigationItem.USER_REGISTRATION);
        return "register-user";
    }

    @PostMapping("/register")
    public String processRegisterUserForm(Model model,
                                          @Valid @ModelAttribute("user") UserDTO userDTO,
                                          BindingResult result,
                                          @RequestParam(required = false) String passwordRetyped
    ) {


        // SecurityConfig.bankClerkUsername is reserved even if there is no such user i DB
        if (SecurityConfig.bankClerkUsername.equals(userDTO.getUsername())) {
            model.addAttribute("user", userDTO);
            result.addError(new FieldError("user", "username", "Username " + userDTO.getUsername() + " is already taken, please choose another!"));
            return "register-user";
        }

        if (result.hasErrors()) {
            model.addAttribute("user", userDTO);
            return "register-user";
        }

        if (userManager.getUserByUsername(userDTO.getUsername()).isPresent()) {
            // vec postoji korisnik s ovim imenom
            result.addError(new FieldError("user", "username", "Username " + userDTO.getUsername() + " is already taken, please choose another!"));
            model.addAttribute("user", userDTO);
            return "register-user";
        }


        if (!userDTO.getPassword().equals(passwordRetyped)) {
            result.addError(new FieldError("user", "password", "Password and retyped password do not match!"));
            model.addAttribute("user", userDTO);
            return "register-user";
        }

        try {
            User user = new User(userDTO.getUsername(), passwordEncoder.encode(userDTO.getPassword()));
            user.setFirstName(userDTO.getFirstName());
            user.setLastName(userDTO.getLastName());
            user.setRole(Role.ROLE_USER);
            user = userManager.save(user);
            bankAccountManager.createAccount(user);
            return "registration-success";

        } catch (Exception e) {
            log.error("Failed to register user!", e);
            model.addAttribute("error", "Unpredicted error occurred! Please contact administrator.");
            return "error-page";
        }
    }

}
