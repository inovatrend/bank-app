package com.inovatrend.king.bank.web;


import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.inovatrend.king.bank.domain.BankAccount;
import com.inovatrend.king.bank.domain.BankTransaction;
import com.inovatrend.king.bank.domain.TransactionStatus;
import com.inovatrend.king.bank.domain.User;
import com.inovatrend.king.bank.dto.BankTransactionDTO;
import com.inovatrend.king.bank.dto.TransactionOrderDTO;
import com.inovatrend.king.bank.service.BankAccountManager;
import com.inovatrend.king.bank.service.BankTransactionManager;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/user")
@SessionAttributes("transaction")
@Slf4j
public class UserTransactionController {

    private final BankAccountManager bankAccountManager;

    private final BankTransactionManager bankTransactionManager;

    public UserTransactionController(BankAccountManager bankAccountManager, BankTransactionManager bankTransactionManager) {
        this.bankAccountManager = bankAccountManager;
        this.bankTransactionManager = bankTransactionManager;
    }


    @GetMapping("/transaction/order")
    public String showTransactionOrderForm(Model model) {
        model.addAttribute("transaction", new TransactionOrderDTO());
        model.addAttribute("selectedNavItem", NavigationItem.MAKE_TRANSACTION);
        return "make-transaction";
    }

    @PostMapping("/transaction/order")
    public String processTransactionOrderForm(Model model,
                                              @Valid @ModelAttribute("transaction") TransactionOrderDTO transactionOrderDTO,
                                              BindingResult result, RedirectAttributes redirectAttrs,
                                              @AuthenticationPrincipal User user) {

        if (result.hasErrors()) {
            model.addAttribute("transaction", transactionOrderDTO);
            return "make-transaction";
        }

        BankAccount debtorAccount = bankAccountManager.getAccount(user);

        Optional<BankAccount> creditorAccount = bankAccountManager.getAccountByIban(transactionOrderDTO.getCreditorIban());
        if (!creditorAccount.isPresent()) {
            result.addError(new FieldError("transaction", "creditorIban", "Creditor iban doesn't exist!"));
            model.addAttribute("transaction", transactionOrderDTO);
            return "make-transaction";
        }

        try {
            bankTransactionManager.transactionOrder(debtorAccount, creditorAccount.get(), transactionOrderDTO.getAmount());
            redirectAttrs.addFlashAttribute("infoMessage", "Transaction successfully submitted!");
            return "redirect:/user/transaction/list";
        } catch (Exception e) {
            log.error("Failed to process transaction!", e);
            model.addAttribute("errorMessage", e.getMessage());
            return "make-transaction";
        }
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    @GetMapping("/transaction/list")
    public String listTransactions(@RequestParam("transactionStatus") Optional<TransactionStatus> transactionStatus,
                                   @RequestParam("page") Optional<Integer> page,
                                   @AuthenticationPrincipal User user,
                                   Model model) {

        int currentPage = page.orElse(1);

        PageRequest pageRequest = TransactionsPageUtils.createPageRequest(currentPage);

        Page<BankTransaction> transactions;
        if (transactionStatus.isPresent()) {
            transactions = bankTransactionManager.getUserTransactionsByStatus(user, transactionStatus.get(), pageRequest);
        } else {
            transactions = bankTransactionManager.getUserTransactions(user, pageRequest);
        }

        List<BankTransactionDTO> transactionDTOS = transactions.stream().map(BankTransaction::toDTO).collect(Collectors.toList());
        model.addAttribute("transactions", transactionDTOS);
        model.addAttribute("transactionStatus", transactionStatus.orElse(null));

        int totalPages = transactions.getTotalPages();
        TransactionsPageUtils.addPageInfo(model, transactions, totalPages);

        model.addAttribute("selectedNavItem", NavigationItem.LIST_TRANSACTIONS);

        return "list-transactions";
    }

}
