package com.inovatrend.king.bank.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.inovatrend.king.bank.domain.BankAccount;
import com.inovatrend.king.bank.domain.BankTransaction;
import com.inovatrend.king.bank.domain.Role;
import com.inovatrend.king.bank.domain.User;

import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class BankTransactionManagerTest {

    @Autowired
    private TransactionHelper transactionHelper;

    @Autowired
    private BankTransactionManager bankTransactionManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserManager userManager;

    @Autowired
    private BankAccountManager bankAccountManager;

    private ExecutorService executorService = Executors.newFixedThreadPool(20);

    private BankAccount debtorAccount;

    private BankAccount creditorAccount;


    @Before
    public void initTest() {
        this.debtorAccount = createBankAccount("test1", "test1");
        this.creditorAccount = createBankAccount("test2", "test2");
    }

    private BankAccount createBankAccount(String username, String password) {
        User user = new User(username, passwordEncoder.encode(password));
        user.setFirstName(username);
        user.setLastName(username);
        user.setRole(Role.ROLE_USER);
        user = userManager.save(user);
        return bankAccountManager.createAccount(user);
    }

    @Test
    public void testTransactions() throws Exception {

//        Assertions.assertThat(testBlacklistEntry.getUsername()).isEqualTo(DEFAULT_USERNAME);

        Assert.assertEquals("Debtor account balance is not " + BankAccountManager.initialBalance, 0, debtorAccount.getBalance().compareTo(BankAccountManager.initialBalance));

        Assert.assertEquals("Creditor account balance is not " + BankAccountManager.initialBalance, 0, creditorAccount.getBalance().compareTo(BankAccountManager.initialBalance));

        for (int i = 0; i < 1000; i++) {
            executorService.execute(() -> transactionHelper.withTransaction(
                    () -> {
                        try {
                            BankTransaction bankTransaction = bankTransactionManager.transactionOrder(debtorAccount, creditorAccount, new BigDecimal(1));
                            bankTransactionManager.commitTransaction(bankTransaction.getId());
                        } catch (BankTransactionException ignored) {
//                            log.error(e.getMessage(), e);
                        }
                    }));
        }

        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }

        Assert.assertEquals("Debtor account balance should be 0.00 ", new BigDecimal(0), debtorAccount.getBalance());

        Assert.assertEquals("Creditor account balance should be 2000.00 ", new BigDecimal(2000), creditorAccount.getBalance());


    }

}
